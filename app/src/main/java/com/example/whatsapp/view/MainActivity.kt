package com.example.whatsapp.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.whatsapp.R
import com.example.whatsapp.view.adapter.FragmentAdapterPage
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewpager_main.adapter = FragmentAdapterPage(supportFragmentManager)
        tabs_main.setupWithViewPager(viewpager_main)
    }
}