package com.example.whatsapp.presenter.status

import com.example.whatsapp.model.Status

interface StatusBindView {

    fun onSuccess(msg: String,  status: List<Status?>?)
}