package com.example.whatsapp.presenter.chat

import com.example.whatsapp.model.Chat

interface ChatBindView {
    fun onSuccessBind(msg: String, chat: List<Chat?>?)
}