package com.example.whatsapp.presenter.calls

import com.example.whatsapp.model.Calls


interface CallBindView {
    fun onSuccessBind(msg: String, chat: List<Calls?>?)
}