package com.example.whatsapp.presenter.calls

import com.example.whatsapp.data.CallData


class CallPresenter(val bindView: CallBindView) {

    fun getCallData(){
        val dataCall = CallData

        bindView.onSuccessBind("Success Bind Data", dataCall.listData)
    }
}