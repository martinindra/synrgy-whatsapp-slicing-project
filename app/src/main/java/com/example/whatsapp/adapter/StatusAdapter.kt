package com.example.whatsapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.whatsapp.R
import com.example.whatsapp.model.Status
import kotlinx.android.synthetic.main.status_list_item.view.*

class StatusAdapter(var data: List<Status>) : RecyclerView.Adapter<StatusAdapter.ViewHolder>() {
    inner class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {
        val iamge = itemView.avatarStatus
        val name = itemView.nameStatus
        val time = itemView.timeStatus
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.status_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.name.text = data?.get(position)?.name
        holder.time.text = data?.get(position)?.time

        Glide.with(holder.itemView.context).load(data?.get(position)?.avatar).into(holder.iamge)
    }

    override fun getItemCount(): Int = data?.size ?: 0
}