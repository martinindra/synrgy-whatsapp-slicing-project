package com.example.whatsapp.data

import com.example.whatsapp.R
import com.example.whatsapp.model.Chat

object ChatData {
    private val image: Array<Int> =
        arrayOf(
            R.drawable.user1,
            R.drawable.user2,
            R.drawable.user3,
            R.drawable.user4,
            R.drawable.user5,
            R.drawable.user6,
            R.drawable.user7,
            R.drawable.user8,
            R.drawable.user9,
            R.drawable.user10
        )

    private val name: Array<String> =
        arrayOf(
            "Andi",
            "Indra Brookman",
            "Jackie Chan",
            "The Rock",
            "SPG Oli",
            "Brother",
            "Eminem",
            "Younglex",
            "Sugar Dedi",
            "Adam Levine"
        )

    private val message: Array<String> =
        arrayOf(
            "Makan Bang",
            "Anjir Lah",
            "Gelut mamank, sekarang gue tunggu",
            "Ngegym Bro",
            "Oli nya bang",
            "Ikkeh bro",
            "Summa lumma dumma",
            "Makan Bang",
            "Sini ku kasih Iphone",
            "Sugar"
        )

    private val time: Array<String> =
        arrayOf(
            "07.43",
            "07.32",
            "07.00",
            "Kemarin",
            "Kemarin",
            "Kemarin",
            "Kemarin",
            "03/05/21",
            "03/05/21",
            "02/05/21r"
        )


    val listData: ArrayList<Chat>
        get() {
            val list : ArrayList<Chat> = arrayListOf<Chat>()
            for (i in image.indices){
                val chat = Chat()
                chat.avatar = image[i]
                chat.name = name[i]
                chat.message = message[i]
                chat.time = time[i]
                list.add(chat)
            }
            return list
        }
}